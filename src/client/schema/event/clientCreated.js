const { DownstreamEvent } = require('ebased/schema/downstreamEvent')

class ClientCreatedEvent extends DownstreamEvent {
  constructor (payload, meta) {
    super({
      type: 'CLIENT.CLIENT_CREATED',
      specversion: 'v1.0.0',
      payload,
      meta,
      schema: {
        strict: false,
        dni: { type: String, required: true },
        birthDate: { type: String, required: true }
      }
    })
  }
}
module.exports = { ClientCreatedEvent }
