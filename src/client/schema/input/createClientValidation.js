const { InputValidation } = require('ebased/schema/inputValidation')
// My Dependencies
const { calculateAgeClient } = require('../../helpers/calculateAgeClient')

class CreateClientValidation extends InputValidation {
  constructor (payload, meta) {
    super({
      type: 'CLIENT.CREATE_CLIENT',
      specversion: 'v1.0.0',
      source: meta.source,
      payload,
      schema: {
        name: { type: String, required: true },
        lastName: { type: String, required: false },
        dni: { type: String, required: true },
        birthDate: {
          type: String,
          required: true,
          custom: (birthDate) => {
            const age = calculateAgeClient(birthDate)
            if (age < 18 || age > 65) {
              return 'User does not comply with policies'
            }
            return true
          }
        }
      }
    })
  }
}

module.exports = { CreateClientValidation }
