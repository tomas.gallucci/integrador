const getKeyFromEnv = require('../helpers/environment')
const { CardService } = require('../services/card.service')
const { ClientService } = require('../services/client.service')
const { calculateAgeClient } = require('../helpers/calculateAgeClient')
const { CARD_TYPE } = require('../const')
const { CreateCardValidation } = require('../schema/input/createCardValidation')
const TABLE_NAME = getKeyFromEnv('clientsDBTable')

module.exports = async (eventPayload, eventMeta) => {
  const eventMapped = new CreateCardValidation(JSON.parse(eventPayload.Message), eventMeta)
  const { dni, birthDate } = eventMapped.get()

  // Service Sync
  const cardService = new CardService()
  const clientService = new ClientService(builderCardUser)

  const card = Math.floor(Math.random() * 10) < 5 ? cardService.generateVisaCard() : cardService.generateMasterCard()
  const clientAge = calculateAgeClient(birthDate)
  try {
    await clientService.setCard(dni, card, clientAge < 45 ? CARD_TYPE.CLASSIC : CARD_TYPE.GOLD)
    return {
      statusCode: 200,
      body: 'Credit Card created successfully'
    }
  } catch (err) {
    return {
      statusCode: 400,
      body: err.message
    }
  }
}

function builderCardUser (userId, creditCard, type) {
  const { cardCode, cardExpires, cardNumber } = creditCard
  const { month, year } = cardExpires
  return {
    TableName: TABLE_NAME,
    Key: {
      id: userId
    },
    UpdateExpression: 'set creditCard = :creditCard',
    ExpressionAttributeValues: {
      ':creditCard': {
        cvv: cardCode,
        expire: `${month.padStart(2, 0)}/${year.slice(2, year.length)}`,
        number: cardNumber,
        type
      }
    }
  }
}
