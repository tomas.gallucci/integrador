const { FaultHandled } = require('ebased/util/error')

const { ClientService } = require('../services/client.service')
const { giftChoser } = require('../helpers/giftChoice')
const getKeyFromEnv = require('../helpers/environment')
const { ERRORS_LAYERS, ERROR_CODE } = require('../const')
const { CreateGiftValidation } = require('../schema/input/createGiftValidation')

const TABLE_NAME = getKeyFromEnv('clientsDBTable')

module.exports = async (eventPayload, eventMeta) => {
  const eventMapped = new CreateGiftValidation(JSON.parse(eventPayload.Message), eventMeta)

  const user = eventMapped.get()

  let gift = null
  try {
    gift = giftChoser(user.birthDate)
  } catch (err) {
    throw new FaultHandled(err.message, { code: ERROR_CODE.GIFT.SELECT, layer: ERRORS_LAYERS.GIFT_CHOICE })
  }

  const clientService = new ClientService(buildParam)
  let dbUser = {}
  if (user.id) {
    dbUser = await clientService.findById(user.id)
  }

  try {
    await clientService.setGift(user.dni || dbUser.dni, gift)

    return {
      statusCode: 200,
      body: user.id ? 'Update Gift successfully' : 'Create Gift successfully'
    }
  } catch (err) {
    return {
      statusCode: 400,
      body: err.message
    }
  }
}

function buildParam (userId, gift) {
  return {
    TableName: TABLE_NAME,
    Key: {
      id: userId
    },
    UpdateExpression: 'set gift = :gift',
    ExpressionAttributeValues: {
      ':gift': gift
    }
  }
}
