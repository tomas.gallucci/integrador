const { FaultHandled } = require('ebased/util/error')

const { ClientCreatedEvent } = require('../schema/event/clientCreated')
const { CreateClientValidation } = require('../schema/input/createClientValidation')
const { SnsService } = require('../services/sns.service')
const { ClientService } = require('../services/client.service')
const getKeyFromEnv = require('../helpers/environment')
const { ERRORS_LAYERS, ERROR_CODE } = require('../const')

const TABLE_NAME = getKeyFromEnv('clientsDBTable')

module.exports = async (commandPayload, commandMeta) => {
  // Input validations
  // eslint-disable-next-line no-new
  const payload = new CreateClientValidation(commandPayload, commandMeta).get()

  const { dni, name, lastName, birthDate } = payload

  const user = {
    dni,
    name,
    lastName,
    birthDate
  }
  // Service Sync
  const clientService = new ClientService(builderUser)
  try {
    await clientService.newUser(user)
  } catch (err) {
    return {
      status: 406,
      body: {
        message: err.message
      }
    }
  }
  const clientEvent = {
    ...user,
    message: `Client with dni: ${dni} created successfull`
  }

  // Service Async
  const snsService = new SnsService('clientCreatedSNS')

  try {
    await snsService.publish(new ClientCreatedEvent(clientEvent, commandMeta))
    return {
      status: 200,
      body: {
        message: 'Client created successfully'
      }
    }
  } catch (err) {
    throw new FaultHandled(err.message, { code: ERROR_CODE.CLIENT.PUBLISH, layer: ERRORS_LAYERS.SNS_PUBLISH })
  }
}

function builderUser (user = {}) {
  return {
    TableName: TABLE_NAME,
    Item: {
      id: user.id,
      dni: user.dni,
      firstName: user.name,
      lastName: user.lastName,
      birthDate: user.birthDate
    }
  }
}
