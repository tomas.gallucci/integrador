const dynamo = require('ebased/service/storage/dynamo')
const { randomUUID } = require('node:crypto')
const getKeyFromEnv = require('../helpers/environment')
const TABLE_NAME = getKeyFromEnv('clientsDBTable')

// My Dependences
const { SnsService } = require('./sns.service')

class ClientService {
  constructor (builder) {
    this.builder = builder
    this.snsService = new SnsService()
  }

  async findByDNI (dni) {
    const input = {
      TableName: TABLE_NAME,
      ExpressionAttributeValues: {
        ':dni': dni
      },
      FilterExpression: 'dni = :dni'
    }

    const result = await dynamo.scanTable(input)

    return result.Items
  }

  async getAll () {
    const params = this.builder()

    const dbResult = await dynamo.scanTable(params)
    const usersAvailables = dbResult.Items.filter((user) => !user.disabled)
    return {
      items: usersAvailables
    }
  }

  async findById (id) {
    const input = {
      TableName: TABLE_NAME,
      Key: {
        id
      }
    }

    const user = await dynamo.getItem(input)

    if (user.Item.disabled) {
      throw new Error('User is disabled')
    }

    return user.Item
  }

  // Create a new user.
  async newUser (user = {}) {
    const [userExists] = await this.findByDNI(user.dni)

    if (userExists) {
      throw new Error('User already exists with this dni')
    }

    user.id = randomUUID()

    const params = this.builder(user)

    return await dynamo.putItem(params)
  }

  async update (user = {}) {
    const params = this.builder(user)

    return await dynamo.updateItem(params)
  }

  async setGift (dni, gift) {
    const [userExists] = await this.findByDNI(dni)

    if (!userExists) {
      throw new Error('User not exits')
    }

    const params = this.builder(userExists.id, gift)
    return await dynamo.updateItem(params)
  }

  // Update a new user
  async setCard (dni, creditCard, type) {
    const [userExists] = await this.findByDNI(dni)

    if (!userExists) {
      throw new Error('User not exits')
    }

    const params = this.builder(userExists.id, creditCard, type)

    return await dynamo.updateItem(params)
  }

  async deleteClientById (id) {
    const params = this.builder(id)

    const user = await dynamo.updateItem(params)

    return user.Item
  }
}

module.exports = { ClientService }
