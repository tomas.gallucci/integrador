const sns = require('ebased/service/downstream/sns')
const getKeyFromEnv = require('../helpers/environment')

class SnsService {
  constructor (key) {
    this.clientTopic = key
  }

  async publish (clientCreatedEvent) {
    const topicArn = getKeyFromEnv(this.clientTopic)
    console.log(this.clientTopic)
    const { eventPayload, eventMeta } = clientCreatedEvent.get()

    const snsPublishParams = {
      TopicArn: topicArn,
      Message: eventPayload
    }

    console.log(snsPublishParams)

    await sns.publish(snsPublishParams, eventMeta)
  }
}

module.exports = { SnsService }
