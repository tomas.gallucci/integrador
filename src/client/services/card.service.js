const { CardFactory } = require('../helpers/card.factory')

class CardService {
  constructor () {
    this.cardFactory = new CardFactory()
  }

  generateVisaCard () {
    return this.cardFactory.generateSingle('visa')
  }

  generateMasterCard () {
    return this.cardFactory.generateSingle('mastercard')
  }
}

module.exports = { CardService }
