const date = new Date()

const CREDIT_CARD_VENDOR = {
  VISA: {
    digitCount: 16,
    prefixes: ['4539', '4556', '4916', '4532', '4929', '4485', '4716', '4']
  },
  MASTERCARD: {
    digitCount: 16,
    prefixes: ['51', '52', '53', '54', '55']
  }
}

const ERRORS_LAYERS = {
  DYNAMO_COMMAND: 'DYNAMO_COMMAND',
  GIFT_CHOICE: 'GIFT_CHOICE',
  SNS_PUBLISH: 'SNS_PUBLISH'
}

const ERROR_CODE = {
  GIFT: {
    CREATE: 'GIFT.CREATE_ERROR',
    SELECT: 'GIFT.SELECT_ERROR'
  },
  CARD: {
    CREATE: 'CARD.CREATE_ERROR'
  },
  CLIENT: {
    CREATE: 'CLIENT.CREATE_ERROR',
    PUBLISH: 'PUBLISH_ERROR'
  }
}

const SEASSON_MONTHS = {
  autumn: {
    start: new Date(`03-21-${date.getFullYear()} 00:00:00`),
    end: new Date(`06-20-${date.getFullYear()} 23:59:59`)
  },
  winter: {
    start: new Date(`${date.getFullYear()}-06-21 00:00:00`),
    end: new Date(`${date.getFullYear()}-09-20 23:59:59`)
  },
  spring: {
    start: new Date(`${date.getFullYear()}-09-21 00:00:00`),
    end: new Date(`${date.getFullYear()}-12-20 23:59:59`)
  },
  summer: {
    start: new Date(`${date.getFullYear()}-12-21 00:00:00`),
    end: new Date(`${date.getFullYear() + 1}-03-20 23:59:59`)
  }
}

const GIFT_CHOICE = {
  autumn: 'diver',
  winter: 'sweater',
  spring: 'shirt',
  summer: 't-shirt'
}

const CARD_TYPE = {
  GOLD: 'gold',
  CLASSIC: 'classic'
}

module.exports = {
  CREDIT_CARD_VENDOR,
  CARD_TYPE,
  SEASSON_MONTHS,
  GIFT_CHOICE,
  ERRORS_LAYERS,
  ERROR_CODE
}
