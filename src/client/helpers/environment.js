const config = require('ebased/util/config')

const env = {
  clientCreatedSNS: 'tomasGallucci-integrador-dev-clientCreatedSNS',
  clientUpdatedSNS: 'tomasGallucci-integrador-dev-clientUpdatedSNS',
  clientsDBTable: 'tomasGallucci-integrador-dev-CLIENTS_TABLE'
}

module.exports = (key) => {
  if (process.env.NODE_ENV === 'debug') {
    return env[key]
  }
  return config.get(key)
}
