const date = new Date()

const { SEASSON_MONTHS, GIFT_CHOICE } = require('../const')

function giftChoser (birthDate) {
  const birth = new Date(birthDate)
  const dateTime = new Date(`${date.getFullYear()}-${birth.getMonth()}-${birth.getDate()}`)

  let gift = null

  for (const key in SEASSON_MONTHS) {
    const seasson = SEASSON_MONTHS[key]
    if (dateTime.getTime() >= seasson.start && seasson.end >= dateTime.getTime()) {
      gift = GIFT_CHOICE[key]
      return gift
    }
  }

  throw new Error('Could not set a gift')
}

module.exports = { giftChoser }
