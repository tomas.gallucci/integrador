const { CREDIT_CARD_VENDOR } = require('../const')

class CardFactory {
  generateSingle (vendor) {
    vendor = vendor.toUpperCase()
    if (!CREDIT_CARD_VENDOR[vendor]) {
      throw new Error("[CreditCardGenerator] Unknown credit card vendor '" + vendor + "'")
    }

    return this.generateWithPreset(CREDIT_CARD_VENDOR[vendor])
  }

  generateWithPreset (preset) {
    const prefix = preset.prefixes[Math.floor(Math.random() * preset.prefixes.length)]
    const numberWithPrefix = prefix + this.generateRandomNumber(preset.digitCount)
    const checksum = this.calculateChecksum(numberWithPrefix)

    const randomExpires = {
      year: `${new Date().getFullYear() + Math.floor(Math.random() * 4) + 1}`,
      month: `${Math.floor(Math.random() * 12) + 1}`
    }
    const fullCard = numberWithPrefix + checksum
    const cardCode = fullCard.slice(preset.digitCount + 1, fullCard.length)
    return {
      cardNumber: fullCard.slice(0, preset.digitCount),
      cardCode,
      cardExpires: randomExpires
    }
  }

  generateRandomNumber (length) {
    let cardNumber = ''

    while (cardNumber.length < length - 1) {
      cardNumber += Math.floor(Math.random() * 10)
    }

    return cardNumber
  }

  calculateChecksum (cardNumber) {
    const reversedCardNumber = this.reverseString(cardNumber)
    const reversedCardNumberArray = reversedCardNumber.split('')
    let sum = 0

    for (let i = 1; i < reversedCardNumberArray.length; i++) {
      sum += parseInt(reversedCardNumberArray[i]) + parseInt(reversedCardNumberArray[i - 1])
    }

    return ((Math.floor(sum / 10) + 1) * 10 - sum) % 10
  }

  reverseString (string) {
    const stringParts = string.split('')
    const reverseArray = stringParts.reverse()

    return reverseArray.join('')
  }
}

module.exports = { CardFactory }
