service: tomasGallucci-integrador
frameworkVersion: '3'

provider:
  name: aws
  region: us-east-1
  stage: dev
  runtime: nodejs18.x
  environment:
    LOG_LEVEL: info
    clientCreatedSNS: !Ref clientCreatedSNS
    clientUpdatedSNS: !Ref clientUpdatedSNS
    createCardQueue: !Ref createCardQueue
    createGiftQueue: !Ref createGiftQueue
    clientsDBTable: !Ref clientsDynamo

functions:
  CreateClient:
    handler: src/client/handler/createClient.handler
    iamRoleStatements:
      - Effect: 'Allow'
        Action:
          - 'dynamodb:Scan'
          - 'dynamodb:PutItem'
        Resource:
          Fn::GetAtt:
            - clientsDynamo
            - Arn
      - Effect: 'Allow'
        Action:
          - 'sns:Publish'
        Resource: 'arn:aws:sns:${self:provider.region}:*:${self:service}-${self:provider.stage}-clientCreatedSNS'
    events:
      - http:
          path: clients
          method: POST
          cors: true
  ListClient:
    handler: src/client/handler/listClients.handler
    iamRoleStatements:
      - Effect: 'Allow'
        Action:
          - 'dynamodb:Scan'
        Resource:
          Fn::GetAtt:
            - clientsDynamo
            - Arn
    events:
      - http:
          path: clients
          method: GET
          cors: true
  GetClientById:
    handler: src/client/handler/getClientById.handler
    iamRoleStatements:
      - Effect: Allow
        Action:
          - 'dynamodb:GetItem'
        Resource:
          Fn::GetAtt:
            - clientsDynamo
            - Arn
    events:
      - http:
          path: /clients/{id}
          request:
            parameters:
              paths:
                id: true
          method: get
          cors: true
  DeleteClientById:
    handler: src/client/handler/getClientById.handler
    iamRoleStatements:
      - Effect: Allow
        Action:
          - 'dynamodb:GetItem'
          - 'dynamodb:PutItem'
        Resource:
          Fn::GetAtt:
            - clientsDynamo
            - Arn
    events:
      - http:
          path: /clients/{id}
          request:
            parameters:
              paths:
                id: true
          method: PATCH
          cors: true
  UpdateClient:
    handler: src/client/handler/updateClient.handler
    iamRoleStatements:
      - Effect: 'Allow'
        Action:
          - 'dynamodb:GetItem'
          - 'dynamodb:UpdateItem'
        Resource:
          Fn::GetAtt:
            - clientsDynamo
            - Arn
      - Effect: 'Allow'
        Action:
          - 'sns:Publish'
        Resource: 'arn:aws:sns:${self:provider.region}:*:${self:service}-${self:provider.stage}-clientUpdatedSNS'
    events:
      - http:
          path: clients
          method: PUT
          cors: true

  CreateGift:
    handler: src/client/handler/createGift.handler
    iamRoleStatements:
      - Effect: 'Allow'
        Action:
          - 'dynamodb:Scan'
          - 'dynamodb:UpdateItem'
        Resource:
          Fn::GetAtt:
            - clientsDynamo
            - Arn
      - Effect: 'Allow'
        Action:
          - 'sqs:ReceiveMessage'
          - 'sqs:GetQueueAttributes'
        Resource:
          Fn::GetAtt:
            - createGiftQueue
            - Arn
    events:
      - sqs:
          arn:
            Fn::GetAtt:
              - createGiftQueue
              - Arn
      - sqs:
          arn:
            Fn::GetAtt:
              - updateGiftQueue
              - Arn
  CreateCard:
    handler: src/client/handler/createCard.handler
    iamRoleStatements:
      - Effect: 'Allow'
        Action:
          - 'dynamodb:Scan'
          - 'dynamodb:UpdateItem'
        Resource:
          Fn::GetAtt:
            - clientsDynamo
            - Arn
      - Effect: 'Allow'
        Action:
          - 'sqs:ReceiveMessage'
          - 'sqs:GetQueueAttributes'
        Resource:
          Fn::GetAtt:
            - createCardQueue
            - Arn
    events:
      - sqs:
          arn:
            Fn::GetAtt:
              - createCardQueue
              - Arn
      - sqs:
          arn:
            Fn::GetAtt:
              - updateCardQueue
              - Arn

resources:
  Resources:
    clientCreatedSNS:
      Type: 'AWS::SNS::Topic'
      Properties:
        DisplayName: clientCreatedSNS
        TopicName: ${self:service}-${self:provider.stage}-clientCreatedSNS
        Subscription:
          - Endpoint:
              Fn::GetAtt:
                - 'createCardQueue'
                - 'Arn'
            Protocol: sqs
          - Endpoint:
              Fn::GetAtt:
                - 'createGiftQueue'
                - 'Arn'
            Protocol: sqs
    clientUpdatedSNS:
      Type: 'AWS::SNS::Topic'
      Properties:
        DisplayName: clientUpdateSNS
        TopicName: ${self:service}-${self:provider.stage}-clientUpdatedSNS
        Subscription:
          - Endpoint:
              Fn::GetAtt:
                - 'updateCardQueue'
                - 'Arn'
            Protocol: sqs
          - Endpoint:
              Fn::GetAtt:
                - 'updateGiftQueue'
                - 'Arn'
            Protocol: sqs

    createCardQueue:
      Type: 'AWS::SQS::Queue'
      Properties:
        QueueName: ${self:service}-${self:provider.stage}-createCardQueue
    createGiftQueuePolicy:
      Type: AWS::SQS::QueuePolicy
      Properties:
        Queues:
          - Ref: createGiftQueue
        PolicyDocument:
          Statement:
            - Effect: Allow
              Principal:
                AWS: '*'
              Action:
                - sqs:SendMessage
                - sqs:ReceiveMessage
                - sqs:DeleteMessage
                - sqs:GetQueueUrl
                - sqs:GetQueueAttributes
                - sqs:ChangeMessageVisibility
              Resource:
                - !GetAtt createGiftQueue.Arn

    createGiftQueue:
      Type: 'AWS::SQS::Queue'
      Properties:
        QueueName: ${self:service}-${self:provider.stage}-createGiftQueue
    createCreditCardQueuePolicy:
      Type: AWS::SQS::QueuePolicy
      Properties:
        Queues:
          - Ref: createCardQueue
        PolicyDocument:
          Statement:
            - Effect: Allow
              Principal:
                AWS: '*'
              Action:
                - sqs:SendMessage
                - sqs:ReceiveMessage
                - sqs:DeleteMessage
                - sqs:GetQueueUrl
                - sqs:GetQueueAttributes
                - sqs:ChangeMessageVisibility
              Resource:
                - !GetAtt createCardQueue.Arn

    updateGiftQueue:
      Type: 'AWS::SQS::Queue'
      Properties:
        QueueName: ${self:service}-${self:provider.stage}-updateGiftQueue
    updateCreditCardQueuePolicy:
      Type: AWS::SQS::QueuePolicy
      Properties:
        Queues:
          - Ref: updateCardQueue
        PolicyDocument:
          Statement:
            - Effect: Allow
              Principal:
                AWS: '*'
              Action:
                - sqs:SendMessage
                - sqs:ReceiveMessage
                - sqs:DeleteMessage
                - sqs:GetQueueUrl
                - sqs:GetQueueAttributes
                - sqs:ChangeMessageVisibility
              Resource:
                - !GetAtt updateCardQueue.Arn

    updateCardQueue:
      Type: 'AWS::SQS::Queue'
      Properties:
        QueueName: ${self:service}-${self:provider.stage}-updateCardQueue
    updateGiftQueuePolicy:
      Type: AWS::SQS::QueuePolicy
      Properties:
        Queues:
          - Ref: updateCardQueue
        PolicyDocument:
          Statement:
            - Effect: Allow
              Principal:
                AWS: '*'
              Action:
                - sqs:SendMessage
                - sqs:ReceiveMessage
                - sqs:DeleteMessage
                - sqs:GetQueueUrl
                - sqs:GetQueueAttributes
                - sqs:ChangeMessageVisibility
              Resource:
                - !GetAtt updateCardQueue.Arn

    clientsDynamo:
      Type: 'AWS::DynamoDB::Table'
      DeletionPolicy: Retain
      Properties:
        AttributeDefinitions:
          - AttributeName: 'id'
            AttributeType: 'S'
        KeySchema:
          - AttributeName: 'id'
            KeyType: 'HASH'
        ProvisionedThroughput:
          ReadCapacityUnits: 1
          WriteCapacityUnits: 1
        StreamSpecification:
          StreamViewType: 'NEW_AND_OLD_IMAGES'
        TableName: ${self:service}-${self:provider.stage}-CLIENTS_TABLE

plugins:
  - serverless-iam-roles-per-function
  - serverless-webpack

package:
  individually: true
